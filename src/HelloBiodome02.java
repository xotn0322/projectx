public class HelloBiodome02 extends Main{
    public static double sum(String[] args) {
        double sun = 0;
        double earth = 0;
        double wind = 0;
        double sum = 0;

        sun = Double.parseDouble(args[0]);
        wind = Double.parseDouble(args[1]);
        earth = Double.parseDouble(args[2]);

        sum = sun + earth + wind;

        return sum;
    }
}
